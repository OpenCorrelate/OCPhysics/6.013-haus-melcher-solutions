#! /bin/sh

for i in $(seq -f "%02g" 1 15)
do
  mkdir -p chapter$i/{scratch,exercises}
  touch chapter$i/{README.md,exercises/README.md}
done
