# Notes and Exercise Solutions to *Electromagnetic Fields and Energy* by Herman. A. Haus and James R. Melcher

Electrodynamics time. Running through the [text](http://web.mit.edu/6.013_book/www/book.html) for 6.013 as I took it. [OCW](https://ocw.mit.edu) has [another version](https://ocw.mit.edu/resources/res-6-001-electromagnetic-fields-and-energy-spring-2008/) with PDF'd chapters. We'll also do some [problem sets](https://ocw.mit.edu/courses/electrical-engineering-and-computer-science/6-013-electromagnetics-and-applications-spring-2009/assignments/) from the 2009 class. Maybe others as I come across them.

## Chapters

1. [Maxwell's Integral Laws in Free Space ](chapters/chapter01/README.md) ([solutions](chapters/chapter01/exercises/README.md))

## AUTHORS

Prez Cannady ([gitlab](https://gitlab.com/revprez))
